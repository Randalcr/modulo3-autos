$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
$('.carousel').carousel({
    interval: 1500
})  
$('#prueba-manejo').on('show.bs.modal', function (e) {
    console.log ("El modal contacto se está mostrando");
    $('#reservaBtn').prop('disabled',true);
    $('#reservaBtn').addClass('btn-secundary'); 
 })
 $('#prueba-manejo').on('shown.bs.modal', function (e) {
     console.log ("El modal contacto se mostró")
 })
 $('#prueba-manejo').on('hide.bs.modal', function (e) {
     console.log ("El modal contacto se oculta")
 })
 $('#prueba-manejo').on('hidden.bs.modal', function (e) {
     console.log ("El modal contacto se ocultó");
     $('#reservaBtn').prop('disabled',false);
     $('#reservaBtn').addClass('btn-primary');
 })